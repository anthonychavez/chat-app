$(document).ready(function(){
    function getRequestObject()
    {
        if (window.XMLHttpRequest) return new XMLHttpRequest();
        else if (window.ActiveXObject)
            return new ActiveXObject("Microsoft.XMLHTTP");

        alert("Your browser does not support AJAX!");
        return null;
    }

    function sanitize(message)
    {
        message = message.replace(/</g, "&lt;");
        message = message.replace(/>/g, "&gt;");
        message = message.trim();
        return message;
    }

    function post(msg)
    {
        let valid = true;
        // let msg = document.getElementById("msgBoxInput").value;
        msg = msg.trim();
        if (msg === "" || msg === null)
        {
            alert("You can't send an empty message.");
            valid = false;
            return valid;
        }
        let xhttp = getRequestObject();
        xhttp.onreadystatechange = function(){
            if (this.readyState == 4 && this.status == 200)
            {
                // let msg = document.getElementById("msgBoxInput");
                let msg = $("#msgBoxInput");
                // console.log(msg.value);
            }
        }
        xhttp.open("POST", "chat.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("msg=" + msg);
        // console.log(xhttp);
        return valid;
    }
	//Open a WebSocket connection.
	var wsUri = "ws://192.168.1.27:8090/WebSocketHandler.php";
	websocket = new WebSocket(wsUri);

    //Connected to server
	websocket.onopen = function(ev) {
		console.log('Connected to WebSocket server ');
	}

    //Connection close
	websocket.onclose = function(ev) {
    	console.log('Disconnected');
        let msg = "You have been disconnected from the websocket server"
        $("#chatBox").append(msg);
    };

    //Message Receved
	websocket.onmessage = function(ev) {
        console.log(ev.data);
        $("#chatBox").append(ev.data);
    };

    //Error
	websocket.onerror = function(ev) {
    	console.log('Error ' + ev.data);
        let msg = "An unknown error occured. Try refreshing the page. <br>";
        $("#chatBox").append(msg);
    };

     //Send a Message
	$('#sendMsg').click(function(){
        let message = $("#msgBoxInput").val();
        if (message != "" || message != null)
        {
            if (post(message))
            {
                message = sanitize(message);
                let htmlMessage = "<div class=\"msg\"><span class=\"user\">You </span>" + ": " + message + "</div>";
                $("#chatBox").append(htmlMessage);
                websocket.send(message);
                $("#msgBoxInput").val("");
            }
        }
	});
     //Send a Message on hitting the enter key
	$('#msgBoxInput').keypress(function(e){
        let kcode = e.keyCode || e.which;
        let message = $("#msgBoxInput").val();
        if (kcode == 13)
        {
            if (message != "" || message != null)
            {
                if (post(message))
                {
                    message = sanitize(message);
                    let htmlMessage = "<div class=\"msg\"><span class=\"user\">You </span>" + ": " + message + "</div>";
                    $("#chatBox").append(htmlMessage);
                    websocket.send(message);
                    $("#msgBoxInput").val("");
                }
            }
        }
	});
});

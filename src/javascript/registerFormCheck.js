// Javascript is used to check the same errors
// PHP checks so that the user doesn't have
// to wait to recieve an error message,
// they get it right away.
// It makes for a better user experience
// and if the user modifies the javascript
// to bypass the errors, PHP can check them.
function checkForm(e)
{
    let valid = true;
    let uname = $("#usernameBox").val();
    uname = uname.trim();
    if (uname === "" || uname === null || uname == " ")
    {
        e.preventDefault();
        let error = $(".error");
        error.html("A username is required");
        valid = false;
        return valid;
    }
    valid = true
    return valid;
}
function checkInput()
{
    let uname = $("#usernameBox");
    uname.keyup(function(){
        let valid = true;
        let regex = /^[a-zA-Z 0-9]+$/;
        let uname = $("#usernameBox");
        let error = $(".error");
        if (regex.test(uname.val()) == false && uname.value != "")
        {
            valid = false;
        }
        else
        {
            valid = true;
        }

        if (valid == false)
        {
            error.html("Only letters, numbers, and white space allowed.");
        }
        else
        {
            error.html("");
        }
        if (uname.val().length > 8)
        {
            error.html("There's an 8 character limit.");
        }
    });
}
checkInput();
$("#register").submit(function(e){checkForm(e);});

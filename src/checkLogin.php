<?php
    function loggedIn()
    {
        $isLoggedIn = true;
        if (!isset($_SESSION["username"]))
        {
            $isLoggedIn = false;
        }
        return $isLoggedIn;
    }
?>

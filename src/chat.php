<?php
    session_start();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
            <meta name = "author" content = "">
            <meta name = "description" content = "">
            <meta name = "keywords" content = "">
            <meta charset = "UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <script src="javascript/jquery-3.4.1.min.js"></script>
            <script src="javascript/websockets.js"></script>
    </head>
    <body>
        <?php
            require "checkLogin.php";
            require "logMessages.php";
            require "clearLog.php";
            require "displayOldMessages.php";

            if (!loggedIn())
            {
                header("Location: index.php");
            }
            $username = $_SESSION["username"];
            echo("<h1 id=\"welcome\">Welcome " . $username . "!</h1>");

            // This stops any errors showing up
            // when the user first enters the
            // page and the chatBox is empty.
            // This well only run on a form submit (A new message is posted)
            // and only if that form submit is a POST request.
            if ($_SERVER["REQUEST_METHOD"] == "POST")
            {
                // Removes white space from both sides
                // of a message, like the post
                // function does.
                // This helps in checking if a message is empty
                // or not, before posting it.
                // It's a little reduntant, but
                // I couldn't think of a better way of doing it.
                $_POST["msg"] = trim($_POST["msg"]);

                if (isset($_POST["msg"]) && $_POST["msg"] != "" && $_POST["msg"] != " ")
                {

                    // Send username to websocket server.
                    $socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket.");
                    socket_connect($socket, "127.0.0.1", 9000) or die("Could not connect to 127.0.0.1:9000");
                    socket_send($socket, $username, strlen($username), 0) or die("Could not send username to websocket server.");
                    // removes unwanted white space, slashes and other
                    // unwanted characters from $_POST["msg"] and posts
                    // it to the chat.
                    logMessage($_POST["msg"]);
                    socket_close($socket);
                }
                else
                {
                    echo("<script>alert(\"You can't send an empty message.\");</script>");
                }
            }
        ?>
        <a href="http://chat.anthonychavez.xyz"><button class="btn">Back</button></a>
        <br>
        <div id="chatBox">
            <?php
                $log = "log.html";
                if (file_exists($log) && filesize($log) > 0)
                {
                    // This should wipe the file when it reaches 5MB.
                    if (filesize("log.html") >= 5000000)
                    {
                        clearLog($log);
                    }
                    displayOldMessages($log);
                }
            ?>
        </div>
        <!-- <form id="chatForm" action="<?php // echo(htmlspecialchars($_SERVER["PHP_SELF"])); ?>" method="post"> -->
            <div id="chatForm">
                <input id="msgBoxInput" type="text" name="msg" form="chatForm" autocomplete="off">
                <!-- <input id="sendMsg" class="btn" type="submit" value="Send" form="chatForm"> -->
                <button id="sendMsg" class="btn">Send</button>
            </div>
        <!-- </form> -->
    </body>
</html>

<?php
    namespace ChatApplication;

    use Ratchet\MessageComponentInterface;
    use Ratchet\ConnectionInterface;

    class WebsocketHandler implements MessageComponentInterface
    {
        protected $clients;

        public function __construct()
        {
            $this->clients = new \SplObjectStorage;
        }

        public function onOpen(ConnectionInterface $conn)
        {
            // Stores connection for later.
            $this->clients->attach($conn);

            echo("New connection $conn->resourceId \n");
        }
        public function onMessage(ConnectionInterface $from, $msg)
        {
            // Create socket to receiver username from chat.php
            $socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket.");
            socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
            socket_bind($socket, "127.0.0.1", 9000) or die("Could not bind socket.");
            socket_listen($socket, 1024);
            $client = socket_accept($socket);
            $username = socket_read($client, 8);
            echo("Username: " . $username . "\n");

            $numRecv = count($this->clients) - 1;
            echo(sprintf("Connection %d sending message '%s' to %d other connection%s ". "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? "" : "s"));

            foreach ($this->clients as $client)
            {
                if ($from != $client)
                {
                    // The sender is not the receiver
                    // send to everyone else instead.
                    $msg = trim($msg);
                    $msg = stripslashes(htmlspecialchars($msg));
                    $msgToSend = "<div class=\"msg\"><span class=\"user\"> " . $username . "</span>" . ": " . $msg . "</div>";
                    $client->send($msgToSend);
                }
            }
            @socket_close($socket);
            @socket_close($client);
        }
        public function onClose(ConnectionInterface $conn)
        {
            $this->clients->detach($conn);

            echo("Connection $conn->resourceId has disconnected \n");
        }
        public function onError(ConnectionInterface $conn, \Exception $e)
        {
            echo("<script>console.log('An error has occurred: $e->getMessage() \n');</script>");

            $conn->close();
        }
    }
?>

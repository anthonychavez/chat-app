<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
            <meta name = "author" content = "Anthony Chavez">
            <meta name = "description" content = "Chatroom ">
            <meta name = "keywords" content = "chat">
            <meta charset = "UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <script src="javascript/jquery-3.4.1.min.js"></script>
    </head>
    <body>
        <?php
            $errorStr = NULL;
            $username = NULL;
            function test_input($data)
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST")
            {
                if (empty($_POST["usernameBox"]))
                {
                    $errorStr = "A username is required";
                }
                else
                {
                    $username = test_input($_POST["usernameBox"]);
                    if (!preg_match("/^[a-zA-Z 0-9]*$/", $username))
                    {
                      $errorStr = "Only letters, numbers, and white space allowed";
                    }
                    else
                    {
                        $_SESSION["username"] = $username;
                        if (strlen($_SESSION["username"]) > 8)
                        {
                            $errorStr = "There's an 8 character limit.";
                        }
                        else
                        {
                            header("Location: chat.php");
                            exit; // Stops the script from continueing to execute
                            // after a location change.
                        }
                    }
                }
            }

        ?>
        <noscript><p>Javascript is needed for this web application to work properly.</p></noscript>
        <div id="registerBox">
        <form id="register" action="<?php echo(htmlspecialchars($_SERVER["PHP_SELF"])); ?>" method="post">
                <h1>Enter a username</h1>
                <span class="error"> <?php echo($errorStr); ?></span><br>
                <input id="usernameBox" type="text" name="usernameBox" form="register" value="<?php echo($username); ?>" autocomplete="off">
                <input class="btn"type="submit" value="Submit" form="register">
            </form>
        </div>
        <script src="javascript/registerFormCheck.js"></script>
    </body>
</html>
